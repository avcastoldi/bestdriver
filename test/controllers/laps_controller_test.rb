require 'test_helper'

class LapsControllerTest < ActionController::TestCase
  setup do
    @lap = laps(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:laps)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create lap" do
    assert_difference('Lap.count') do
      post :create, lap: { acceleration: @lap.acceleration, anticipation: @lap.anticipation, average_consumption: @lap.average_consumption, average_speed: @lap.average_speed, distance: @lap.distance, duration: @lap.duration, finished_at: @lap.finished_at, gear_changes: @lap.gear_changes, score: @lap.score, started_at: @lap.started_at, total_consumption: @lap.total_consumption, without_consumption: @lap.without_consumption }
    end

    assert_redirected_to lap_path(assigns(:lap))
  end

  test "should show lap" do
    get :show, id: @lap
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @lap
    assert_response :success
  end

  test "should update lap" do
    patch :update, id: @lap, lap: { acceleration: @lap.acceleration, anticipation: @lap.anticipation, average_consumption: @lap.average_consumption, average_speed: @lap.average_speed, distance: @lap.distance, duration: @lap.duration, finished_at: @lap.finished_at, gear_changes: @lap.gear_changes, score: @lap.score, started_at: @lap.started_at, total_consumption: @lap.total_consumption, without_consumption: @lap.without_consumption }
    assert_redirected_to lap_path(assigns(:lap))
  end

  test "should destroy lap" do
    assert_difference('Lap.count', -1) do
      delete :destroy, id: @lap
    end

    assert_redirected_to laps_path
  end
end
