# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150219151520) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "laps", force: true do |t|
    t.integer  "person_id"
    t.decimal  "distance"
    t.decimal  "duration"
    t.integer  "score"
    t.decimal  "acceleration"
    t.decimal  "gear_changes"
    t.decimal  "anticipation"
    t.decimal  "average_consumption"
    t.decimal  "total_consumption"
    t.decimal  "average_speed"
    t.decimal  "without_consumption"
    t.datetime "started_at"
    t.datetime "finished_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name"
  end

  add_index "laps", ["person_id"], name: "index_laps_on_person_id", using: :btree

  create_table "people", force: true do |t|
    t.string   "name"
    t.string   "email"
    t.string   "photo_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "team_id"
  end

  add_index "people", ["team_id"], name: "index_people_on_team_id", using: :btree

  create_table "teams", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "widgets", force: true do |t|
    t.string   "name"
    t.text     "description"
    t.integer  "stock"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
