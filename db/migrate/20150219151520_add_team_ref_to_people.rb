class AddTeamRefToPeople < ActiveRecord::Migration
  def change
    add_reference :people, :team, index:true
  end
end
