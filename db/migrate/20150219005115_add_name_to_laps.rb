class AddNameToLaps < ActiveRecord::Migration
  def change
    add_column :laps, :name, :string
  end
end
