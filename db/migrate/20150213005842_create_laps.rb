class CreateLaps < ActiveRecord::Migration
  def change
    create_table :laps do |t|
      t.belongs_to :person, index: true
      t.decimal :distance
      t.decimal :duration
      t.integer :score
      t.decimal :acceleration
      t.decimal :gear_changes
      t.decimal :anticipation
      t.decimal :average_consumption
      t.decimal :total_consumption
      t.decimal :average_speed
      t.decimal :without_consumption
      t.datetime :started_at
      t.datetime :finished_at

      t.timestamps
    end
  end
end
