class LapsController < ApplicationController
  before_action :set_lap, only: [:show, :edit, :update, :destroy, :checkout]

  # GET /laps
  # GET /laps.json
  def index
    @laps = Lap.joins(:person).order("people.name")
  end

  # GET /laps/1
  # GET /laps/1.json
  def show
  end

  # GET /laps/new
  def new
    @lap = Lap.new
  end

  # GET /laps/1/edit
  def edit
  end

  # POST /laps
  # POST /laps.json
  def create
    @lap = Lap.new(lap_params)

    respond_to do |format|
      if @lap.save
        format.html { redirect_to laps_url, notice: 'Lap was successfully created.' }
        format.json { render :show, status: :created, location: @lap }
      else
        format.html { render :new }
        format.json { render json: @lap.errors, status: :unprocessable_entity }
      end
    end
  end
  
  #POST /laps/checkin/1
  def checkin
    @lap=Lap.new(started_at: Time.now)
    @lap.person = Person.find(params[:id])
    @lap.save

    respond_to do |format|
      format.js
    end
  end
  
  #POST /laps/checkout/1
  def checkout
    @lap.finished_at = Time.now
    @lap.save
    respond_to do |format|
      format.html {redirect_to edit_lap_path(@lap)}
    end
  end


  # PATCH/PUT /laps/1
  # PATCH/PUT /laps/1.json
  def update
    respond_to do |format|
      if @lap.update(lap_params)
        format.html { redirect_to laps_url, notice: 'Lap was successfully updated.' }
        format.json { render :show, status: :ok, location: @lap }
      else
        format.html { render :edit }
        format.json { render json: @lap.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /laps/1
  # DELETE /laps/1.json
  def destroy
    @lap.destroy
    respond_to do |format|
      format.html { redirect_to laps_url, notice: 'Lap was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_lap
      @lap = Lap.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def lap_params
      params.require(:lap).permit(:person_id, :distance, :duration, :score, :acceleration, :gear_changes, :anticipation, :average_consumption, :total_consumption, :average_speed, :without_consumption, :started_at, :finished_at)
    end
end
