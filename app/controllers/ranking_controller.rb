class RankingController < ApplicationController
  
  def index
    @drivers = Lap.order(average_consumption: :desc)[0..4]
    @teams = Team.top(5)
    @laps = Lap.order(average_consumption: :desc)
    @fastest = Lap.order(average_speed: :desc)[0..2]
    @without_consumption = Lap.order(without_consumption: :desc)[0..2]
  end
end
