class Team < ActiveRecord::Base
    has_many :people, inverse_of: :team, dependent: :destroy
    accepts_nested_attributes_for :people, allow_destroy: true
    
    def score
        people.inject(0) {|s,p| s += (((l = p.laps.first).nil? ? 0 : l.average_consumption.nil?)? 0: l.average_consumption) }
    end
    
    def self.top(n=1)
        l=self.all
        n=l.size if n==:all
        l=l.sort { |a,b| a.score <=> b.score}
        l.reverse!
        l2=[]
        l.each {|i| l2 << i if i.score > 0}
        l2[0..n-1]
    end
end
