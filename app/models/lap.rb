class Lap < ActiveRecord::Base
  belongs_to :person
  #before_save :update_attributes
  
  def << (message)
    @previous, @last = @last, message
    @first = message if @first.nil?
    update_attributes
  end

  def update_attributes
    self.distance = distance_traveled
    self.total_consumption - fuel_spent
  end

  private
  
    def fuel_spent
      @last.fuel_level_displayed - @first.fuel_level_displayed
    end

    def distance_traveled
      @last.distance_totalizer - @first.distance_totalizer
    end

end
