require 'digest/md5'

class Person < ActiveRecord::Base
  has_many :laps, dependent: :destroy
  belongs_to :team, inverse_of: :people

  
  def avatar_url
    photo_url == "" && gravatar_url || photo_url
  end
  
  def gravatar_url
    "http://www.gravatar.com/avatar/#{Digest::MD5.hexdigest(email)}"
  end
  
  def <<
  end
end
