json.array!(@laps) do |lap|
  json.extract! lap, :id, :distance, :duration, :score, :acceleration, :gear_changes, :anticipation, :average_consumption, :total_consumption, :average_speed, :without_consumption, :started_at, :finished_at
  json.url lap_url(lap, format: :json)
end
