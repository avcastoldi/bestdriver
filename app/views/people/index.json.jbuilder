json.array!(@people) do |person|
  json.extract! person, :id, :name, :email, :photo_url
  json.url person_url(person, format: :json)
end
