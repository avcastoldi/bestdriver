class Message

  def initialize(line)
    @a=line.split(',')
  end
  
  def timestamp
    if @timestamp.nil?
      d=@a[6..11].collect{|s| s.to_i }
      d[0]+=2000
      @timestamp=DateTime.new(*d)
    end
    @timestamp   
  end
  
  def body
    @body ||= @a[21..43].collect{|s| ('00000000'+s.to_i(16).to_s(2))[-8..-1]}.inject(""){|s,a| s << a }
  end
  
  def signal(position, size, resolution, offset)
    body[position, size].to_i(2) * resolution + offset 
  end
  
  def brake_info_status
    case signal(0, 3, 1, 0)
    when 1
      i=:brake_pedal_not_pressed
    when 2
      i=:brake_pedal_pressed
    when 4
      i=:brake_pedal_confirmed_pressed
    else
      i=:unavailable
    end
  end

  def veihcle_locked_from_outside?
    signal(3, 1, 1, 0) == 1
  end
     
  def flashing_indicator
    case signal(4, 2, 1, 0)
    when 1
      :left
    when 2
      :right
    else
      :off
    end
  end
  
  def cloutch_switch_minimum_travel?
    signal(8,2,1,0) == 2
  end
    
  def cloutch_switch_maximum_travel?
    signal(10,2,1,0) == 2
  end
  
  def distance_totalizer
    signal(12,28,0.01,0)
  end

  def vehicle_speed
    signal(40,16,0.01,0)
  end
  
  def fuel_level_displayed
    signal(120,7,1,0)
  end

  def fuel_autonomy_in_km
    signal(127,10,1,0)
  end
  
  def external_temp
    signal(137,8,1,-40)
  end
  
  def fluent_driving_indicator
    signal(145,7,1,0)
  end
  
  def acceleration_indicator
    signal(152,7,1,0)
  end
  
  def gsi_respect
    signal(159,7,1,0)
  end
  
  def engine_rpm
    signal(160,16,0.125,0)
  end
  
  def nominal_fuel_consumption
    0
  end
  
end