class EmailProcessor
  def initialize(email)
    @email = email
  end
  
  def process
    # all of your application-specific code here - creating models,
    # processing reports, etc
    
    #logger.debug "Incoming email from #{@email.from}, subject: #{@email.subject}"
    
    @lap=Lap.new(name: @email.subject)
    email.attachments.each {|f| parse f}
    @lap.save
    
  end
  
  def parse(file)
    File.open(file) do |f|
      while line = file.gets
        @lap << Message.new(line)
      end
    end
  end
end