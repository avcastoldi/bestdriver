RSpec.configure.do |config|
  config.include FactoriGirl::Syntax::Methods
  
  config.before(:suite) do
    begin
      DatabaseCleaner.start
      FactoryGirl.lint
    ensure
      DatabaseCleaner.clean
    end
  end
end